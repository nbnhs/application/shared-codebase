"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
class Applicant {
    constructor(id) {
        this.id = id;
    }
    static getDatabaseEntryProperties(ID) {
        return __awaiter(this, void 0, void 0, function* () {
            console.log(`Getting database entry for ${ID}`);
            const url = `relay.nbnhs.club?operation=get&type=Applicant&ID=${ID}`;
            return (yield getURL(url));
        });
    }
    name() {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield Applicant.getDatabaseEntryProperties(this.id);
            return [data.first_name, data.last_name];
        });
    }
    getPropertyFromDatabase(name) {
        return __awaiter(this, void 0, void 0, function* () {
            const json = yield Applicant.getDatabaseEntryProperties(this.id);
            return json[name];
        });
    }
}
Applicant.validQueries = [
    "signature",
    "leadership",
    "service",
    "teacher"
];
exports.Applicant = Applicant;
function getURL(url) {
    return __awaiter(this, void 0, void 0, function* () {
        const response = yield fetch(`https://cors-anywhere.herokuapp.com/${url}`);
        if (response.ok) {
            return yield response.json();
        }
        else {
            throw new Error("Invalid response!");
        }
    });
}
exports.getURL = getURL;
// https://gist.github.com/hunan-rostomyan/28e8702c1cecff41f7fe64345b76f2ca
function getCookie(name) {
    const nameLenPlus = name.length + 1;
    return (document.cookie
        .split(";")
        .map((c) => c.trim())
        .filter((cookie) => {
        return cookie.substring(0, nameLenPlus) === `${name}=`;
    })
        .map((cookie) => {
        return decodeURIComponent(cookie.substring(nameLenPlus));
    })[0] || null);
}
exports.getCookie = getCookie;
