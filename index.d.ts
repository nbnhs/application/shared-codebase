interface IDatabaseEntry {
    id: string;
    first_name: string;
    last_name: string;
    leadership: string;
    service: string;
    teacher: string;
    signature: string;
}
export declare class Applicant {
    id: string;
    static validQueries: string[];
    static getDatabaseEntryProperties(ID: string): Promise<IDatabaseEntry>;
    constructor(id: string);
    name(): Promise<string[]>;
    getPropertyFromDatabase(name: string): Promise<string>;
}
export declare function getURL(url: string): Promise<object>;
export declare function getCookie(name: string): string;
export {};
