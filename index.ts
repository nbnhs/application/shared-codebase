interface IDatabaseEntry {
  // Represents a Google Cloud Datastore Entity of kind "Applicant".
  id: string;
  first_name: string;
  last_name: string;
  leadership: string;
  service: string;
  teacher: string;
  signature: string;
}

export class Applicant {
  public static validQueries = [
    "signature",
    "leadership",
    "service",
    "teacher"
  ];

  public static async getDatabaseEntryProperties(
    ID: string
  ): Promise<IDatabaseEntry> {
    console.log(`Getting database entry for ${ID}`);
    const url = `relay.nbnhs.club?operation=get&type=Applicant&ID=${ID}`;
    return (await getURL(url)) as IDatabaseEntry;
  }

  constructor(public id: string) {}

  public async name(): Promise<string[]> {
    const data = await Applicant.getDatabaseEntryProperties(this.id);
    return [data.first_name, data.last_name];
  }

  public async getPropertyFromDatabase(name: string): Promise<string> {
    const json = await Applicant.getDatabaseEntryProperties(this.id);
    return json[name];
  }
}

export async function getURL(url: string): Promise<object> {
  const response = await fetch(`https://cors-anywhere.herokuapp.com/${url}`);

  if (response.ok) {
    return await response.json();
  } else {
    throw new Error("Invalid response!");
  }
}

// https://gist.github.com/hunan-rostomyan/28e8702c1cecff41f7fe64345b76f2ca
export function getCookie(name: string): string {
  const nameLenPlus = name.length + 1;
  return (
    document.cookie
      .split(";")
      .map((c) => c.trim())
      .filter((cookie) => {
        return cookie.substring(0, nameLenPlus) === `${name}=`;
      })
      .map((cookie) => {
        return decodeURIComponent(cookie.substring(nameLenPlus));
      })[0] || null
  );
}
